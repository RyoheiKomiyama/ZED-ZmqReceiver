#include "testApp.h"

#include "ofxZmq.h"

ofxZmqSubscriber subscriber;
ofxZmqPublisher publisher;

//--------------------------------------------------------------
void testApp::setup()
{
    // load zedconfig.txt
    std::ifstream ifsconfig("../src/zedconfig.txt");
    std::string strconfig;
    for(int i=0; i<4; i++){
        getline(ifsconfig, strconfig);
        if(i==1){
            if(strconfig=="VGA"){
                width = 640;
                height = 480;
            }
            else if(strconfig=="HD720"){
                width = 1280;
                height = 720;
            }
            else if(strconfig=="HD1080"){
                width = 1920;
                height = 1080;
            }
            else if(strconfig=="HD2K"){
                width = 2208;
                height = 1242;
            }
        }
        else if(i==3){
            if(strconfig=="uchar") zeddatatype = ZedDataType::UCHAR;
            else if(strconfig=="float") zeddatatype = ZedDataType::FLOAT;
        }
    }
    
    //load intrinsicparameters.txt
    std::ifstream ifsparam("../src/intrinsicparameters.txt");
    std::string strparam;
    for(int i=0; i<8; i++){
        getline(ifsparam, strparam);
        if(i==1) fx = std::stod(strparam);
        else if(i==3) fy = std::stod(strparam);
        else if(i==5) cx = std::stod(strparam);
        else if(i==7) cy = std::stod(strparam);
    }
    
	// start client
	subscriber.connect("tcp://192.168.11.31:9999");
    
    
    mesh.setUsage(GL_DYNAMIC_DRAW);
    mesh.setMode(OF_PRIMITIVE_POINTS);
    
    cam.setAutoDistance(false);
    cam.setDistance(200);
}

//--------------------------------------------------------------
void testApp::update()
{

    //data.clear();
    //subscriber.receive(data);
    //cout << "receive " << to_string(count++) << endl;
    while (subscriber.hasWaitingMessage())
    {
        data.clear();
        subscriber.getNextMessage(data);
        
        cout << "receive " << to_string(count++) << endl;
    }
    
    float minD = 0; // 0[mm]
    float maxD = 1000000; // 1000000[mm]
    
    mesh.clear();
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            float d = ((float*)data.getData())[i*width+j];
            if(d>minD && d<maxD){
                float x = (i-cx)*d/fx;
                float y = -(j-cy)*d/fy;
                ofVec3f p(x,y,d);
                mesh.addVertex(p);
            }
        }
    }

    
    cout << "update fps: " << to_string(ofGetFrameRate()) << endl;
    
}

//--------------------------------------------------------------
void testApp::draw()
{
    
    //ofTexture tex;
    //tex.loadData((unsigned char *)data.getData(), width, height, GL_LUMINANCE);
    //tex.draw(0,0);
    
    ofClear(0);
    
    cout << mesh.getVertices().size() << endl;
    if(mesh.getVertices().size()){
        ofPushStyle();
        {
            glPointSize(1);
            cam.begin();
            {
                ofPushMatrix();
                {
                    //ofTranslate(0, 0, -5000);
                    ofRotate(180,0,1,0);
                    ofRotate(-90,0,0,1);
                    mesh.draw();
                }
                ofPopMatrix();
            }
            cam.end();
        }
        ofPopStyle();
    }
    
    ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), ofGetWidth()-120, 20);

    
    cout << "draw" << endl;

    
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
    for(int i=0; i<720; i++){
        for(int j=0; j<1280; j++){
            cout << ((float*)data.getData())[i*1280+j] << endl;
        }
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y)
{

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo)
{

}
