## ZED-ZmqReceiver
Receiving Depth Data of StereoLab's ZED Camera from ZED-ZmqSender(https://gitlab.com/RyoheiKomiyama/ZED-ZmqSender) running on Windows PC.
Using this, you can use StereoLab's ZED Camera with MacOS.

## with openframeworks
This sample project is generated with openframeworks. You can create your own project easily.
1. generate project with ofxZmq by using project-generator.
2. replace your src directory with this src directory.
3. copy ZED-ZmqSender/src/zedconfig.txt and ZED-ZmqSender/src/intrinsicparameters.txt to this src directory.
4. edit ip address in src/testApp.txt to your windows pc running ZED-ZmqSender.